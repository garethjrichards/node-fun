let total = {
    evenFibonnaci: function (upTo, even) {
        let previousNum = 1;
        let nextNum = 1;
        let total = 0;
        while (nextNum <= upTo) {
            let temp = nextNum;
            nextNum = previousNum + nextNum;
            previousNum = temp;
            if (even && previousNum % 2 === 0) {
                total += previousNum;
            } else if (!even) {
                total += previousNum;
            }
        }
        return total;
    }
};
module.exports = total;
