let multipleDivisors = require('./multipleDivisor.js');
let evenFibonacci = require('./evenFibonacci.js');
let express = require('express');
let app = express();
app.set('view engine', 'ejs');

let bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));

let server = app.listen(5000);

app.get('/', function (req, res) {
    let projects = [
        {
            title: 'Multiple Divisors',
            url: './multiple-divisor'
        },
        {
            title: 'Even Fibonacci Total',
            url: './even-fibonacci'
        }
    ];
    res.render('index', {projects: projects});
});

app.get('/multiple-divisor', function (req, res) {
    res.render('multiple-divisor');
});

app.post('/submit-multiple-divisor', function (req, res) {
   let num = req.body.number;
   let divisors = req.body.divisors.split(',');
   let total = multipleDivisors.multipleIn(num, divisors);
   console.log('(multiple-divisor) - Number: ' + num + ' - Divisors: ' + divisors + ' - Total = ' + total);
   res.render('submit-result', { total: total });
});

app.get('/even-fibonacci', function (req, res) {
    res.render('even-fibonacci');
});

app.post('/submit-even-fibonacci', function (req, res) {
    let max = req.body.max;
    let even = req.body.even;
    let total = evenFibonacci.evenFibonnaci(max, even);
    console.log('(even-fibonacci) - Max: ' + max + ' - Even?: ' + even + ' - Total = ' + total);
    res.render('submit-result', { total : total });
});
