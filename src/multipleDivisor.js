let multiple = {
    multipleIn: function (num, divisibleArray) {
        let total = 0;
        num--;
        while (num > 0) {
            for(let divisor of divisibleArray) {
                if ((num % divisor) === 0) {
                    total += num;
                    break;
                }
            }
            num--;
        }
        return total;
    }
};
module.exports = multiple;
